package com.aaas.models;

public class Usuario {

	private Integer usuarioid;
	private String usuario;
	private String pass;
	
	public Integer getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(Integer usuarioid) {
		this.usuarioid = usuarioid;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
}
