package com.aaas.servlets;

import java.io.Serializable;

public class VOLogin implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9023612345988556807L;

	private Integer adminId;
	private String usuario;
	
	public Integer getAdminId() {
		return adminId;
	}
	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
}
