package com.aaas.servlets;

import java.io.IOException;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import com.aaas.dao.ICInicioSesion;
import com.aaas.dao.CInicioSesionImp;
import com.aaas.models.Usuario;

/**
 * Servlet implementation class LoginServlet
 */
@jakarta.servlet.annotation.WebServlet("/LoginWeb")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public LoginServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Hola: ").append(request.getContextPath());
	}

	/**
	 * @throws ServletException 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// recibo datos del formulario de inicio
		System.out.println("entramos ICInicioSesion");
		String usr = request.getParameter("usuario");
		String pass = request.getParameter("password");
		
		ICInicioSesion ICIniSesion = new CInicioSesionImp();
		
		Usuario usuario = new Usuario();
		usuario = ICIniSesion.validarSesion(usr, pass);
		
		if ( usuario != null) {
			System.out.println("entramos ICInicioSesion");
			// si las credenciales coinciden se otorga acceso
			
			VOLogin vo = new VOLogin();
			
			vo.setAdminId(usuario.getUsuarioid());
			vo.setUsuario(usuario.getUsuario());
			/**
			 * se envía request al jsp para probar que funcione
			 * request.setAttribute("usuarioLogeado", vo);
			*/
			
			// creao la session
			HttpSession session = request.getSession(true);
			session.setAttribute("usuarioLogeado", vo);
			request.getRequestDispatcher("bienvenido.jsp").forward(request, response);
			
		}else {
			System.out.println("error");
			// si las credenciales son incorrecta se redirecciona al inicio
			response.sendRedirect("/aaas");
		}
		
		response.getWriter().append("Served at [POST]: ").append(request.getContextPath());
	}

}
