package com.aaas.dao;

import com.aaas.models.Usuario;

public interface ICInicioSesion {

	public Usuario validarSesion(String usuario, String pass);
}
