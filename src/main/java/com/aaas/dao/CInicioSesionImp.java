package com.aaas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.aaas.conexion.ConexionDB;
import com.aaas.models.Usuario;

public class CInicioSesionImp implements ICInicioSesion {

	@Override
	public Usuario validarSesion(String usr, String pass) {
		Usuario usuario = null;
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			con = new ConexionDB().getConnection();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from usuarios where usuarios = ? and pass = ?");
				ps = con.prepareStatement(sb.toString());
				ps.setString(1, usr);
				ps.setString(2, pass);
				
				rs = ps.executeQuery();
				
				if (rs.next()) {
					usuario = new Usuario();
					usuario.setUsuarioid(rs.getInt("usuariosid"));
					usuario.setUsuario(rs.getString("usuario"));
					usuario.setPass(rs.getString("pass"));
				}
				ps.close();
				rs.close();
				con.close();
		} catch (Exception e) {
			System.out.println("Error al cerrar la conexión a la base de datos: " + e.getMessage());
		}
		return usuario;
	}

}
