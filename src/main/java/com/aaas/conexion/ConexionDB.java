package com.aaas.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionDB {

    public Connection getConnection() {
        Connection conn = null;
        
        String url = "jdbc:postgresql://localhost:5432/bdtsige"; // aquí debes colocar la URL de tu base de datos
        String user = "postgres"; // aquí debes colocar el usuario de tu base de datos
        String password = "1234"; // aquí debes colocar la contraseña de tu base de datos

        try {
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Conexión exitosa a la base de datos");
        } catch (SQLException e) {
            System.out.println("Error al conectarse a la base de datos: " + e.getMessage());
        }
        return conn;
    }
}
