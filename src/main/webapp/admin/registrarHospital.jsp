<%@page import="com.aaas.servlets.VOLogin"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registrar Hospital</title>
</head>
<body>

	<p>Registro de Hospitales: <%= ((VOLogin)session.getAttribute("usuarioLogeado")).getUsuario() %>. </p>

	<a href="/aaas/bienvenido.jsp">Volver</a> | <a href="">Cerrar sesion</a>

	<div class="container">
		<form action="" method="POST">
			<div>
				<div>Alta hospital</div>
				<div>
					<label>Nombre:</label>
					<input type="text" name="nombre" id="nombre" required>
				</div>
				<div>
					<label>Tipo:</label>
 					<select name="tipo">
						<option selected disabled>Elegir...</option>
						<option name="mutualista">Mutualista</option>
						<option name="Seguro privado">Seguro Privado</option>
						<option name="Servicio estatal">Servicio Estatal</option>
					</select>
				</div>
			</div>
			<div>
				<input type="button" name="cancelar" value="Cancelar" >
				<input type="submit" name="enviar" value="Enviar" >
			</div>
		</form>
	</div>
</body>
</html>